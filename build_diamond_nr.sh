#!/bin/bash
#
#SBATCH --job-name=build_db_diamond
#SBATCH --cpus-per-task=6
#SBATCH --mem=6GB
#SBATCH --partition=microbiolprio
#SBATCH --nodelist=micronode4

# Download and build the DIAMOND NCBI BLAST nr database
# Remove old DBs and Rsync new DBs with all nodes
# J. Frank


DB_PATH=/scratch2/databases/
mkdir -p $DB_PATH
RUNDIR=$(mktemp -d -p $DB_PATH $SLURM_JOB_NAME-XXX)
cd $RUNDIR

# Download nr sequence data
wget ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz

# Decompress nr sequence data
pigz -d nr.gz

# Build DIAMOND db
diamond makedb --in nr -d nr -p $SLURM_CPUS_PER_TASK

# Delete sequencing data and old DIAMOND database
rm -rf nr $DB_PATH/diamond

# Rename new database folder
mv $RUNDIR $DB_PATH/diamond

# Set permissions
chmod -R 755 $DB_PATH
chgrp -R microadmin $DB_PATH

# Rsync databases with other micronodes
for NODE in $(sinfo -h --Node --partition=microbiol --format=%N); do
  if [[ "$NODE" != "$SLURM_JOB_NODELIST" ]]; then
    ssh $NODE mkdir -p $DB_PATH/diamond
    rsync -av --delete $DB_PATH/diamond $NODE:$DB_PATH
  fi
done
