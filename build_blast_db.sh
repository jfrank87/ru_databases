#!/bin/bash
#
#SBATCH --job-name=build_db_blast
#SBATCH --cpus-per-task=2
#SBATCH --mem=14GB
#SBATCH --partition=microbiolprio
#SBATCH --nodelist=micronode4

# Download and build NCBI BLAST databases (nt, nr, taxonomy) and Swiss-Prot/UniProt
# Remove old DBs and Rsync new DBs with all nodes
# J. Frank


DB_PATH=/scratch2/databases/
mkdir -p $DB_PATH
RUNDIR=$(mktemp -d -p $DB_PATH $SLURM_JOB_NAME-XXX)
cd $RUNDIR

# Download BLAST databases
# Use all cores for downloading data in parallel - does not generate high load
parallel --gnu "update_blastdb.pl --passive --decompress --blastdb_version 5 {}" ::: nr_v5 nt_v5 taxdb swissprot_v5

# Create database index for nt database (time consuming)
makembindex -input nt_v5 -iformat blastdb

# Delete old BLAST databases
rm -rf $DB_PATH/blast

# Rename new database folder
mv $RUNDIR $DB_PATH/blast

# Set permissions
chmod -R 775 $DB_PATH
chgrp -R microadmin $DB_PATH

# Rsync databases with other micronodes
for NODE in $(sinfo -h --Node --partition=microbiol --format=%N); do 
  if [[ "$NODE" != "$SLURM_JOB_NODELIST" ]]; then
    ssh $NODE mkdir -p $DB_PATH/blast
    rsync -av --delete $DB_PATH/blast $NODE:$DB_PATH
  fi
done
