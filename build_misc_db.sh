#!/bin/bash
#
#SBATCH --job-name=build_db_misc
#SBATCH --cpus-per-task=8
#SBATCH --mem=20GB
#SBATCH --partition=microbiol
#SBATCH --nodelist=micronode4


set -a
set -o errexit;

# Download several databases/indices
# Hope this is stable for a while...
# J. Frank


#--- Select DBs to build ---#
BUILD=silva,mothur,checkm,kaiju,centrifuge,krona  # DBs to build

DB_PATH=/scratch2/databases/
SILVA_RELEASE="132"


mkdir -p $DB_PATH

# Rsync
function do_rsync {
  chmod 775 $DB_PATH
  chgrp -R microadmin $DB_PATH
  for NODE in $(sinfo -h --Node --partition=microbiol --format=%N); do 
    if [[ "$NODE" != "$SLURM_JOB_NODELIST" ]]; then 
      rsync -av --delete $DB_PATH/$1 $NODE:$DB_PATH/$1/
    fi
  done
}

# SILVA
function build_silva {
  mkdir "$DB_PATH"/silva_"$SILVA_RELEASE" && cd "$_"
  wget -r ftp://anonymous@ftp.arb-silva.de/current/Exports/*
  find . -type f -name "*.gz" | parallel --gnu -j "$SLURM_CPUS_PER_TASK" 'pigz -d {}'
  find . -type f -name "*.md5" -delete
  mv "$DB_PATH"/silva_"$SILVA_RELEASE"/ftp.arb-silva.de/current/Exports/* "$DB_PATH"/silva_"$SILVA_RELEASE"
  cd "$DB_PATH"/silva_"$SILVA_RELEASE"; rm -r ftp.arb-silva.de/current/Exports
  do_rsync "silva_$SILVA_RELEASE"
}

# Mothur
function build_mothur {
  mkdir "$DB_PATH"/mothur_tmp && cd "$_"
  wget https://mothur.org/w/images/3/32/Silva.nr_v132.tgz
  wget https://mothur.org/w/images/7/71/Silva.seed_v132.tgz
  cat *.tgz | tar -xvzf - -i; rm *tgz
  rm -rf "$DB_PATH"/mothur
  mv "$DB_PATH"/mothur_tmp "$DB_PATH"/mothur
  do_rsync "mothur"
}

# CheckM
function build_checkm {
  mkdir "$DB_PATH"/checkm_tmp && cd "$_"
  wget https://data.ace.uq.edu.au/public/CheckM_databases/checkm_data_2015_01_16.tar.gz
  tar xzf *.gz; rm *.gz
  rm -rf "$DB_PATH"/checkm
  mv "$DB_PATH"/checkm_tmp "$DB_PATH"/checkm
  do_rsync "checkm"
}

# Kaiju
function build_kaiju {
  mkdir "$DB_PATH"/kaiju_tmp && cd "$_"
  wget http://kaiju.binf.ku.dk/database/kaiju_index_nr_euk.tgz
  tar xzf *.tgz; rm *.tgz
  rm -rf "$DB_PATH"/kaiju
  mv "$DB_PATH"/kaiju_tmp "$DB_PATH"/kaiju
  do_rsync "kaiju"
}

# Centrifuge
function build_centrifuge {
  mkdir "$DB_PATH"/centrifuge_tmp && cd "$_"
  wget ftp://ftp.ccb.jhu.edu/pub/infphilo/centrifuge/data/p_compressed_2018_4_15.tar.gz
  tar xzf *.gz; rm *.gz
  rm -rf "$DB_PATH"/centrifuge
  mv "$DB_PATH"/centrifuge_tmp "$DB_PATH"/centrifuge
  rename 's/p_compressed/prokaryotes_15-04-18/' *.cf
  do_rsync "centrifuge"
}

# Krona
function build_krona {
  mkdir "$DB_PATH"/krona_tmp && cd "$_"
  /usr/local/bioinfo/KronaTools-2.7/updateTaxonomy.sh "$DB_PATH"/krona_tmp
  /usr/local/bioinfo/KronaTools-2.7/updateAccessions.sh "$DB_PATH"/krona_tmp
  rm -rf "$DB_PATH"/krona
  mv "$DB_PATH"/krona_tmp "$DB_PATH"/krona
  mv "$DB_PATH"/krona/accession2taxid/* "$DB_PATH"/krona/; rm -r "$DB_PATH"/krona/accession2taxid/
  do_rsync "krona"
}

# Build databases
parallel -j $SLURM_CPUS_PER_TASK 'build_{}' ::: $(echo $BUILD | tr "," " ")

