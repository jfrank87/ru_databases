#!/bin/bash
#
#SBATCH --job-name=build_db_refseq
#SBATCH --cpus-per-task=32
#SBATCH --mem=40GB
#SBATCH --partition=microbiolprio
#SBATCH --nodelist=micronode4


# Download and build the NCBI RefSeq database
# Remove old DBs and Rsync new DBs with all nodes
# J. Frank

DB_PATH=/scratch2/databases/
mkdir -p $DB_PATH
RUNDIR=$(mktemp -d -p $DB_PATH $SLURM_JOB_NAME-XXX)
cd $RUNDIR

# Prepare jobs for downloading data
curl -l ftp://ftp.ncbi.nlm.nih.gov/refseq/release/bacteria/*.gz >> files_bac.txt
curl -l ftp://ftp.ncbi.nlm.nih.gov/refseq/release/archaea/*.gz >> files_arch.txt
sed -i -e 's#^#ftp://ftp.ncbi.nlm.nih.gov/refseq/release/bacteria/#' files_bac.txt
sed -i -e 's#^#ftp://ftp.ncbi.nlm.nih.gov/refseq/release/archaea/#' files_arch.txt

cat files_bac.txt files_arch.txt > urls_refseq.txt
split -l 20 urls_refseq.txt dl_refseq-
rm files_bac.txt files_arch.txt urls_refseq.txt

# Download RefSeq data
parallel --gnu -j $SLURM_CPUS_PER_TASK 'wget -i {}' ::: dl_refseq*

# Decompress archives
parallel --gnu -j $SLURM_CPUS_PER_TASK 'pigz -d {}' ::: *gz
 
# Create BLAST databases
parallel --gnu -j $SLURM_CPUS_PER_TASK 'makeblastdb -in {} -out {.} -dbtype nucl -parse_seqids -hash_index' ::: *.fna
parallel --gnu -j $SLURM_CPUS_PER_TASK 'makeblastdb -in {} -out {.} -dbtype prot -parse_seqids -hash_index' ::: *.faa

# Remove sequence data
rm -f *.fna *.faa

# Bind BLAST database volumes together (create aliases: *.nal for nucleotide, *.pal for protein)
blastdb_aliastool -dblist "$(ls archaea.*.*.genomic* | rev | cut -d "." -f2- | rev | uniq | tr "\n" " ")" -dbtype nucl -out refseq_archaea_genomic -title refseq_archaea_genomic
blastdb_aliastool -dblist "$(ls archaea.*.rna.* | rev | cut -d "." -f2- | rev | uniq | tr "\n" " ")" -dbtype nucl -out refseq_archaea_rna -title refseq_archaea_rna
blastdb_aliastool -dblist "$(ls archaea.*.protein* | grep 'archaea.[0-9]' | rev | cut -d "." -f2- | rev | uniq | tr "\n" " ")" -dbtype prot -out refseq_archaea_protein -title refseq_archaea_protein
blastdb_aliastool -dblist "$(ls archaea.n*.protein* | rev | cut -d "." -f2- | rev | uniq | tr "\n" " ")" -dbtype prot -out refseq_archaea_nr_protein -title refseq_archaea_nr_protein
 
blastdb_aliastool -dblist "$(ls bacteria.*.*.genomic* | rev | cut -d "." -f2- | rev | uniq | tr "\n" " ")" -dbtype nucl -out refseq_bacteria_genomic -title refseq_bacteria_genomic
blastdb_aliastool -dblist "$(ls bacteria.*.rna.* | rev | cut -d "." -f2- | rev | uniq | tr "\n" " ")" -dbtype nucl -out refseq_bacteria_rna -title refseq_bacteria_rna
blastdb_aliastool -dblist "$(ls bacteria.*.protein* | grep 'bacteria.[0-9]' | rev | cut -d "." -f2- | rev | uniq | tr "\n" " ")" -dbtype prot -out refseq_bacteria_protein -title refseq_bacteria_protein
blastdb_aliastool -dblist "$(ls bacteria.n*.protein* | rev | cut -d "." -f2- | rev | uniq | tr "\n" " ")" -dbtype prot -out refseq_bacteria_nr_protein -title refseq_bacteria_nr_protein

# Index nucleotide database (TIME CONSUMING)
ls *.nal | cut -d "." -f1 | parallel --gnu --j $SLURM_CPUS_PER_TASK 'makembindex -input {} -iformat blastdb'

# Done

# Delete current RefSeq database + data download jobs
rm -rf $DB_PATH/refseq dl_refseq*

# Rename new database
mv $RUNDIR $DB_PATH/refseq

# Set permissions
chmod -R 775 $DB_PATH
chgrp -R microadmin $DB_PATH

# Distribute/synchronize database between nodes
for NODE in $(sinfo -h --Node --partition=microbiol --format=%N); do
  if [[ "$NODE" != "$SLURM_JOB_NODELIST" ]]; then
    ssh $NODE mkdir -p $DB_PATH/refseq
    rsync -av --delete $DB_PATH/refseq $NODE:$DB_PATH/
  fi
done
